const Product = require("../models/Product")

module.exports.addProduct = (data) => {
    if(data.isAdmin){
        let newProduct = new Product ({
            productName: data.product.productName,
            description: data.product.description,
            price: data.product.price
        })
    
        return newProduct.save().then((newProduct, error) => {
            if(error){
                return error
            }
            return "Congratulation Admin! You added a new Product!"
        })

    }
    let message = Promise.resolve({
        message: "User must be ADMIN to access this."         
    })

    return message.then((value) => {
        return value
    })
} 

module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result
    })    
}

module.exports.getActiveProducts = () => {
        return Product.find({isActive: true}).then(result => {
            return result

    })
}

module.exports.getProduct = (productId) => {
    return Product.findById(productId).then(result => {
        return result
    })
}

module.exports.updateProduct = (productId, newData) => {
    if(data.isAdmin){
        return Product.findByIdAndUpdate(productId, {
            productName: newData.productName,
            description: newData.description,
            price: newData.price
    
        }).then((updatedProduct, error) => {
            if(error){
                return false
            }
            return updatedProduct
        })

    }
    let message = Promise.resolve({
        message: "User must be ADMIN to access this."         
    })

    return message.then((value) => {
        return value
    })
}

module.exports.archiveProduct = (productId) => {
    if(productId){
        return Product.findByIdAndUpdate(productId, {
            isActive: false
        })
        .then((archiveProduct, error) => {
            if(error){
                return false
            }
            return true
        })
    }
    let message = Promise.resolve({
        message: "User must be ADMIN to access this."         
    })

    return message.then((value) => {
        return value
    })

}


/*
if(data.isAdmin){
        return Product.find({}).then(result => {
            return result
        })

    }
    let message = Promise.resolve({
        message: "User must be ADMIN to access this."         
    })

    return message.then((value) => {
        return value
    })
}


*/ 