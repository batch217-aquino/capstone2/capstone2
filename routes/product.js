const express = require("express");
const router = express.Router();
const Product = require("../models/Product");
const productController = require("../controllers/productController");
const auth = require ("../auth")


router.post("/createProduct", auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.addProduct(data).then(resultFromController => {
        res.send(resultFromController)
    })
})

router.get("/allProducts", auth.verify, (req, res) => {
    productController.getAllProducts().then(resultFromController => {
        res.send(resultFromController)
    })
})

router.get("/allActiveProducts", auth.verify, (req, res) => {

    productController.getActiveProducts().then(resultFromController => {
        res.send(resultFromController)
    })
})

router.get("/:productId", auth.verify, (req, res) => {
    productController.getProduct(req.params.productId).then(resultFromController => {
        res.send(resultFromController)
    })
})

// Updating a single Product
router.patch("/:productId/update", auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.updateProduct(data).then(resultFromController => {
        res.send(resultFromController)
    })
    productController.updateProduct(req.params.productId, req.body).then(resultFromController => {
        res.send(resultFromController)
    })
})

// Archiving a single Product
router.patch("/:productId/archive", auth.verify, (req, res) => {
    productController.archiveProduct(req.params.productId).then(resultFromController => {
        res.send(resultFromController)
    })
})

module.exports = router